


## Usage

```python
import foobar


# returns 'local'
foobar.pluralize('local')


# returns 'geese'
foobar.pluralize('goose')



# returns 'server'
foobar.singularize('server')

```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.
